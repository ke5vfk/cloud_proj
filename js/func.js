
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

// make sure initial password given when creating account
function createPword(pword)  {
	if(pword.value == "" || pword.value == 0)  {
		alert("Please provide a password.");
		pword.value = "";
		pword.focus();
		return false;
	}
	
	return true;
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------

// confirm that user did not mistype password when creating account
function createPword2(pword2)  {
	var pword = document.getElementById("createP");

	// User supplies a password and then confirms their choice.
	// Make sure the user input the same password choice each time.
	if(pword.value != pword2.value)  {
		alert("Passwords do not match.");
		pword.value = "";
		pword2.value = "";
		pword.focus();
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

function changePword2(pword2)  {
	var pword = document.getElementById("newPword");
	
	// User supplies a new password and then confirms their choice.
	// Make sure the user input the same password choice each time.
	if(pword.value != pword2.value)  {
		alert("Passwords do not match.");
		pword.value = "";
		pword2.value = "";
		pword.focus();
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// make sure password supplied when logging in
function checkPword(pword)  {
	if(pword.value == "" || pword.value == 0)  {
		alert("Please provide a password to login.");
		pword.value = "";
		pword.focus();
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// make sure valid email given
function checkEmail(email)  {
	var valid = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/; 

   	if(valid.test(email.value)){  
	    return true;
   	} 

   	else if (email.value == "" || email.value == 0)  {
	  alert("Please enter a valid email address.\n");
	  email.value = "";
	  email.focus();
	  return false;
   	}

   	else {          
      alert("Please enter a valid email address.\n");
	  email.value = "";
	  email.focus();
	  return false;
   	}
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------



