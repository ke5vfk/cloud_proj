<?php

    // for user session information across pages
    session_start();

    require("account_helpers.php");
    
    if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: login.php"); 
        die;
    } 
    
    // Unset any possible past search info (we're done doing a search if we're here).
    if(isset($_SESSION['post_data']))  {
      unset($_SESSION['post_data']);
    }
  

    $change_password = new ChangePassword;
    $change_password->changePword();
	
	include('nav.php');

?>

<!DOCTYPE html>
<html lang="en">

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

  

	<div class="panel panel-primary pass center-block">
		<div class="panel-heading lead text-uppercase text-center">Change Password</div>
		<div class="panel-body">
		  <form class="form-signin" role="form" method = "post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			  	<div class="form-group">
					<label for="text" > Current Password </label>
					<input type="password" class="form-control" id = "pword" name = "pword" required autofocus><span class="error"> <?php $change_password->getPwordErr();?></span>
				</div>
				<div class="form-group">
					<label for="text" > New Password </label>
					<input type = "password" class = "form-control" id = "newPword" name = "newPword" onchange="createPword(this);" required onchange = "createPword();"><span class = "error"> <?php $change_password->getNewPwordErr();?></span>
				</div>
				<div class="form-group">
					<label for="text" > Confirm New Password </label>
					<input type = "password" class = "form-control" id = "newPword2" name = "newPword2" onchange="changePword2(this);" required onchange = "createPword2();"><span class = "error"> <?php $change_password->getNew2Err();?></span>
				</div>
			  <input type = "submit" class="btn btn-lg btn-primary btn-block" value = "Submit">
		  </form>
		  
		</div>
    </div> <!-- /container -->

    <script src="../js/func.js"></script>
    
  </body>
</html>