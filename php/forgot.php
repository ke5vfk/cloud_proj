<?php

    // for user session information across pages
    session_start();

    require("account_helpers.php");

    $forgot = new ForgotPassword;
    $forgot->forgotPassword();
	
	include('nav.php');
    
?>

<!DOCTYPE html>
<html lang="en">

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

    <div class="container">

		<div class="panel panel-primary login center-block center-all">
			<div class="panel-heading lead text-uppercase text-center ">Account Recovery</div>
			  <div class="panel-body">
				  <form class="form-signin" role="form" method = "post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        <input type="email" class="form-control" id = "email" name = "email" placeholder="Email Address" onchange="checkEmail(this);" required autofocus><span class="error">* <?php echo $forgot->getEmailErr();?></span><br /><br />
					<input type = "submit" class="btn btn-lg btn-primary btn-block" value = "Recover Account" onclick = "checkForgot();">
				  </form>
				</div>
			</div>
		</div> <!-- /container -->
    </div> <!-- /container -->

    <script src="../js/func.js"></script>
    
  </body>
</html>