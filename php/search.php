<?php
  
  // for user session information across pages
  session_start();

  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: login.php"); 
        die;
  } 


  require("form_processing_helpers.php");
  
  
  $search = new Record;
  $search->processSearch();
include('nav.php');


?>
<!DOCTYPE html>
<html lang="en">
  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

	<div class="panel panel-primary search center-block">
		<div class="panel-heading">Search Records</div>
		<p style="" id="instruction"> Please enter your search criteria.</p>

		<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" >
					
		<div>      
		  <table style="padding:25%;">
			 <tbody>
				   
			  <tr>    
				<td>Keywords:</td>
				<td><input type="text" id="keywords" name="keywords" onchange="checkKeywords();" value ="<?php $search->getKeywords(); ?>" autofocus  size = 20><span><?php $search->getKeywordErr();?></span></td>
				<td></td>
			  </tr>
			</tbody>
		  </table> 
		</div>

		<div style="padding:1%;">
				<input class="btn" type="submit" name="submit_search"" id="submit_search" value="Search Research Records">
				<input class="btn" type="button" value="Cancel" onclick="location.href='landing.php'">
		</div>
		</form>
	</div>

<script src="../js/func.js"></script>
    
  </body>
</html>
      
