<?php
  
  // This script will delete an individual file upload from a record.
  
  // for user session information across pages
  session_start();
 
  // If no user session in place, redirect to landing page.
  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: login.php"); 
        die;
  } 

  require("form_processing_helpers.php");
    
    // get the value for which file to delete (this will be a path to the file to delete)
    if(isset($_GET['file']))   {
      $delete_file = checkForm($_GET['file']);
      
      $user = $_SESSION['user'];
      $userID = $_SESSION['userID'];
      
      $user_ID_from_db = 0;
      
      // Get the ID of the record (for privilege check).
      if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)  {
        $id = checkForm($_GET['id']);
        
        // connect to db
        $db = dbConnect();
        
        
        // Need to get the userID of the research record from the research table (must have delete privilege).
        $sql = 'SELECT userID FROM research WHERE researchID = ?';
        $file_privilege = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $file_privilege->execute(array($id));
          
        $row = $file_privilege->fetch(PDO::FETCH_ASSOC);
        $user_ID_from_db = $row['userID'];
      
        // Check to be sure userID and recordID match.
        if($userID == $user_ID_from_db)  {
          // Delete the file.
          if(is_file($delete_file))  {
                  unlink($delete_file);

                  // Redirect back to the record currently being edited.
                  header("Location: record.php?id={$id}"); 
                  die;
          }
        }
        else  {
          die("You do not have the privileges to delete this file.");
        }
      }
    }
?>