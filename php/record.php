<?php
  
  // for user session information across pages
  session_start();

  // Redirect to landing page if no user session in place.
  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: landing.php"); 
        die;
  } 
  
  // Unset any possible past search info (we're done doing a search if we're here).
  if(isset($_SESSION['post_data']))  {
    unset($_SESSION['post_data']);
  }
  
  require("form_processing_helpers.php");

  $record = new Record;
  
  // check to see if we currently have an id -- meaning we are in edit mode for a record.
    if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)   {
        $id = checkForm($_GET['id']);
        $record->setupEdit($id);
    } 
    else if(isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0)  {
      $id = checkForm($_POST['id']);
      $record->setupEdit($id);
    }
  
  $edit_mode = $record->getEditStatus();
  
  $record->processResearchForm();
  $title = "New Submissions";
  include('nav.php');

?>    

<!DOCTYPE html>
<html lang="en">
  <!--head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Research Record</title>

    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

    </style>

  </head-->

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

	<div class="panel panel-primary record center-block">
		<div class="panel-heading lead text-uppercase text-center">Add Research</div>
		<div class="panel-body">
			<form method = "post" enctype="multipart/form-data" role="form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			  <?php if(isset($id))  {
				echo "<input type=\"hidden\" name=\"id\" value= $id>";
			  } ?>
			
				<div class="form-group">
					<label for="text">Research Area Keyword(s):*</label>
					<input type="text" class="form-control" id="keywords" name="keywords" autofocus value ="<?php $record->getKeywords(); ?>" required size = 20><span><?php $record->getKeywordErr();?></span>
        </div>
				<div class="form-group">
					<label for="text">Attachments</label>
					<label class=" btn btn-md btn-primary">Upload File<input class="hidden" type="file" name="attachments[]" id="attachments" multiple><span><?php $record->getAttachmentsUploadErr();?></span> </label>
				</div>
					
					<?php if($edit_mode)  { 
			   
					  echo "<tr>
							  <td>Uploaded Attachments:  </td>
							  <td>";
							  $record->showUploadedAttachments();
					  echo "</td></tr>";
					} ?>

			  <!-- Form management buttons -->
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<input type="submit" name="submit_record" id="submit_record" value="Submit" class="btn btn-primary btn-large">
					</div>
					<div class="btn-group">
						<input type="button" value="Cancel" onclick="location.href = 'landing.php'" class="btn btn-primary btn-large">
					</div>
					<?php if(!$edit_mode)  { ?>
					<div class="btn-group">
					 <input class="btn btn-primary btn-large" type="reset" value="Clear">
					 </div>
					<?php } ?>
				</div>
			 </form>
		</div>
	</div>
  
  <script src="../js/func.js"></script>
    
  </body>
</html>
