<?php
  
  // for user session information across pages
  session_start();

  // Can only view if logged in.
  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: landing.php"); 
        die;
  } 

  require("form_processing_helpers.php");

  $record_view = new RecordView;
  
  $ID = $record_view->getID();
  
  include("nav.php");

?>

<!DOCTYPE html>
<html lang="en">
  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

    <div>
    <h1 align="center" style="text-align:center;">Research Record View</h1>

  <br /><br />
  </div>

      <?php

        // Create a table of all research reports for the user
        $record_view->createRecordView();
        
  ?>
  
 <script src="../js/func.js"></script>
    
  </body>
</html>
      