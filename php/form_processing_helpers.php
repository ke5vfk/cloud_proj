<?php

class Record  {
    var $ID;
    var $errors;
    var $db;
    var $user;
    var $userID;
    var $recordID;
    var $keywords;
    var $keywordErr;
    var $edit = false;
    var $attachmentsMessage;
    var $attachCount;
    var $query_string;
    
    
    // Process the search criteria (make sure user isn't being stupid).
    function processSearch()  {
        // define error checking and container variables and set to empty values          
        $this->errors = 0; 
          
          
        // get current user session
        $this->user = $_SESSION['user'];
        $this->userID = $_SESSION['userID'];
        
        // Connect to db if haven't already.
         if(!$this->db)  {
                $this->db = dbConnect();
         }
 

  		if(isset($_POST['submit_search'])) {
          
          // get form data, making sure it is valid
          $this->processKeywords();
          

          // Only search if no errors (why search for crap???).
          if ($this->errors == 0)  {  		
            // Start building a query string using the search criteria to help us find all the matches.
            $this->find_all_the_things();           
          }
          
      }	      
    }
    
    function find_all_the_things()  {
        
        if(isset($_POST['submit_search'])) {
            $this->query_string = 'SELECT * FROM research WHERE keywords LIKE :keywords';
            
            // Set up a SESSION variable with all the posted input.
            $_SESSION['post_data'] = $_POST;
            
            // Make a SESSION variable to hold the actual SQL query string.
            $_SESSION['post_data']['query_string'] = $this->query_string;                     
            
            // Redirect to a view page to showcase any search hits.
            header("Location:landing.php");
        }
       
    }

    
    // Function to setup initial edit mode.  Post will take care of rest from here-on out.
    function setupEdit($id)  {
        $this->recordID = $id;
        $this->edit = true;
    }
    
    
    // Function to process submission of valid research records.  If invalid, provides error detection.
    function processResearchForm()  {
        $this->attachmentsMessage = "";
  		$this->errors = 0; 
        $this->attachCount = 0;
        $this->keywordErr = "";
        $this->keywords = "";
          
        // get current user session
        $this->user = $_SESSION['user'];
        $this->userID = $_SESSION['userID'];
        
         if(!$this->db)  {// connect to db
                $this->db = dbConnect();
         }
         
         // If we are in edit mode.... 
         if ($this->edit)  {
                
                // Pull in the information (granted the user's ID will allow that record to be edited).
                $sql = 'SELECT * FROM research WHERE researchID = ? AND userID = ?';
                $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $query->execute(array($this->recordID, $this->userID));
                
                $row = $query->fetch(PDO::FETCH_ASSOC);
                $ID = $row['userID'];
               
                
                // If the user does not have appropriate account ID, don't allow the edit and redirect to create new record.
                if($ID != $this->userID)  {
                      $this->edit = false;
                      $this->recordID = 0;
                      header("Location: record.php");
                      die;
                } 
                      
                // Otherwise, pull in former values for display in the edit.  
                $this->keywords = $row['keywords'];
        }
                

  		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          
          // get form data, making sure it is valid
          $this->processKeywords();
         
         
          // If we are in edit mode, get the attachment count.
          // (Still have restrictions on # of uploads.)
          if($this->edit)  {
              $this->getAttachmentsCount();
          }
          
          // Check for errors in files to be uploaded.
          $this->checkAttachments();
      	
          // Only submit or update a record if no errors in inputs or uploads.
          if ($this->errors == 0)  {

                // Update the record if we are doing an edit; create a new record otherwise.
                if ($this->edit)  {
                        $this->updateRecord();
                }
                else  {
                        $this->submitRecord();
                }

                // Upload any files.
                $this->uploadAttachments();
                
                // close the db connection
                $this->db = null;
                
                $this->showSuccess();
                   
        }
      }	 
    }
    
   
    // Notify user that record has been submitted.
    function showSuccess()  {
                        echo"
                        <script>
                            alert('Your record has been saved.');
                            document.location.href = 'landing.php';
                        </script>";
    }

    
    
    // Used to ascertain user can't upload more than the attachments limit.
    function getAttachmentsCount()  {
        // the path for the attachments of the given record
        $path = "../../uploads/{$this->recordID}/attachments/";

        $files = glob("../../uploads/{$this->recordID}/attachments/*.{pdf,PDF,doc,docx,DOC,DOCX,txt,TXT}", GLOB_BRACE);

        if ($files) {
            $filecount = count($files);
            $this->attachCount = $filecount;
        }
    }

    
    // Used to display an uploaded attachments in a view of a particular record.
    function showUploadedAttachments()  {
        // Save the current directory location.
        $current_dir = getcwd();
        
        // the path for the attachments of the given record
        $path = "../../uploads/{$this->recordID}/attachments/";
        
        // Make a copy of the path.
        $copy = $path;
        if(is_dir($path))  {
            // Change to the attachment directory for the record.
            chdir($path);
            
            // Get all attached files with the valid extensions.
            $path = glob("*.{pdf,PDF,TXT,DOCX,DOC,doc,docx,txt}", GLOB_BRACE);
            
            // Display a link to each attachment ($copy contains the path while $attached contains the name--this links to the actual file itself)
            foreach ($path as $attached) {
               echo "<a href=\"{$copy}{$attached}\">$attached</a>";
               echo '<a onclick = "return confirm(\'Are you sure you want to delete this upload?\')" href="delete_record.php?id=' . $this->recordID . '&file=' . $copy . $attached . '">&nbspDelete Upload</a>';
               echo "<br />";
            }
            echo "<br /><br /><br />";
            
            // Restore the directory.
            chdir($current_dir);
        }
    }
    
    // Function to check for valid attachments.
    function checkAttachments()  {
        if(isset($_FILES['attachments']) && $_FILES['attachments']['size'] > 0)  {
  		    $size = count($_FILES['attachments']['name']);
            $allowed = 50 - $size - $this->attachCount;
            
            // Loop through all the attachment uploads -- allow multiple of these.
		    for ($i = 0; $i < $size; $i++) {
                if($_FILES["attachments"]["tmp_name"][$i] != '')  {
                    // Allowed attachment types.
			        $valid_extensions = array("pdf", "PDF", "DOCX", "DOC", "TXT", "txt", "docx", "doc");     
            
                    // Explode file name from dot. 
			        $ext = explode('.', basename($_FILES['attachments']['name'][$i]));   
            
                    // Save the file extension.
			        $file_extension = end($ext); 
                    
                    if($allowed < 0 || $size > 50 || $i >= 50)  {
                        $this->attachmentsMessage .= "Only 50 uploads allowed.  Try again.;";
                        $this->errors += 1;
                        break;
                    }
                    
                    else if($_FILES["attachments"]["size"][$i] > 10000000)  {
                        $this->attachmentsMessage .= $_FILES['attachments']['name'][$i] . ' is too large (100 MB limit per file).;';
                        $this->errors += 1;
                    }
                    else if(!in_array($file_extension, $valid_extensions))  {
                        $this->attachmentsMessage .= $_FILES['attachments']['name'][$i] . ' is an invalid file type (only PDF files allowed).;';
                        $this->errors += 1;
                    }
                }
		      }
        }
    }
    
    function uploadAttachments()  {
        if(isset($_FILES['attachments']) && $_FILES['attachments']['size'] > 0)  {
           if($_FILES['attachments']['tmp_name'][0] != '')  {
                // Directory in which all images will be saved.
                $target_path = "../../uploads/{$this->recordID}/attachments/";    
           
                // Recursively create the target path to save the attachments.
                if(!is_dir($target_path))  {
                    mkdir($target_path, 0777,true);
                }
  		
                // Loop through all the attachment uploads -- allow multiple of these.
		        for ($i = 0, $size = count($_FILES['attachments']['name']); $i < $size && $i < 50; $i++) {
                    $use_this_path = $target_path . $_FILES['attachments']['name'][$i];
				    if (!move_uploaded_file($_FILES['attachments']['tmp_name'][$i], $use_this_path)) {
					      die($_FILES['attachments']['name'][$i] . ' failed to upload. Please try again!');
                    }
		        }
           }
        }
    }
    
    
    
    // Submit the record to our db.
    function submitRecord()  {
        // Attempt to insert into the trademark record into the database.
        $sql = 'INSERT INTO research(userID, username, keywords) 
                VALUES(?, ?, ?)';
        $query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $query->execute(array($this->userID, $this->user, $this->keywords));

        // Get trademark ID for last insert.
        $this->recordID = $this->db->lastInsertId();
        

        if(!$query)  {
            die ("New record insertion failed.");
        }
        else  {
            
        }
    }
    
    
    // Update the record in our db.
    function updateRecord()  {        

        // Attempt to update the record in the database.
        $sql = 'UPDATE research SET keywords = ? WHERE researchID = ?';
        $updateRecord = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $updateRecord->execute(array($this->keywords, $this->recordID));
        

        if(!$updateRecord)  {
            die ("Update to record failed");
        }
    }
    
    function processKeywords()  {
       /* if (empty($_POST['keywords'])) {
            $this->keywordErr = "Please provide keyword(s) for your research area.";
            $this->errors += 1;
        } */
        if (empty($_REQUEST['keywords']))  {
           $this->keywordErr = "Please provide keyword(s) for your research area.";
           $this->errors += 1; 
        }
        else 
            $this->keywords = checkForm($_POST['keywords']);
    }
    
    function getKeywords()  {
        echo $this->keywords;
    }
    
    function getKeywordErr()  {
        echo $this->keywordErr;
    }
    
    // Return the edit status of the current trademark record (T or F).
    function getEditStatus()  {
        return $this->edit;
    }
  
    
    
    // Outputs attachments upload error to screen.
    function getAttachmentsUploadErr()  {
        if($this->attachmentsMessage != "")  {
            // Use semicolon to separate string of errors.
            $pieces = explode(";", $this->attachmentsMessage);
            foreach ($pieces as $piece)  {
                echo $piece;
            }
        }
    }
    
}


// -------------------------------------------------------------------------------------------------------------------------------------------------------

class ResultsView  {
    var $db;
    var $user;
    var $userID;
    var $total_results;
    var $query_string;
    var $search_mode = false;
    var $query;
    
    
    // Used to lookup user credentials so user can only access their records.
    function getUserIDs()  {

        // get current user 
        $this->user = $_SESSION['user'];
        $this->userID = $_SESSION['userID'];        

    }
    
    // If we are currently wanting to display search results, we need to do things differently.
    // Setup for the search by extracting the passed query string to lookup the matches to display.
    // Set the search_mode to true.
    function setupSearch($query_string)  {
        $this->search_mode = true;
        
        $this->db = dbConnect();
        
        // Prepare for the query.
        $this->query = $this->db->prepare($query_string, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $_SESSION['post_data']['keywords'] = "%".$_SESSION['post_data']['keywords']."%";
        $this->query->bindParam(':keywords', $_SESSION['post_data']['keywords']);
    }
    
    function isEmpty() {
        return ($this->total_results == 0);
    }
    
    // Displays record table.
    function createRecordTable()  {
        // Show the search results based on dynamic query prepared above.
        if($this->search_mode)  {          
            $this->query->execute(); 
        }

        // Otherwise show all the marks.
        else  {
            $this->db = dbConnect();
            $sql = 'SELECT * FROM research WHERE userID = ?';
            $this->query = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $this->query->execute(array($this->userID));
        }
        
        if (!$this->query)
            die("Error occurred -- no records found ");
           
        // Created paginated view (10 records viewable per page). 
        $entries_per_page = 10;
        
        // Get the total number of results.
        $this->total_results = $this->query->rowCount();
        
        // Find the total number of pages (will round up).
        $total_pages = ceil($this->total_results / $entries_per_page);
		$_SESSION['total_pages'] = $total_pages;
        
        // See if a page was passed for viewing.
		if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $current_page = checkForm($_GET['page']);

            // Get the bounds.						                
		    if ($current_page > 0 && $current_page <= $total_pages) {
			    $start = ($current_page - 1) * $entries_per_page;
			    $end = $start + $entries_per_page; 
            }
	        else  {
				$start = 0;
			    $end = $entries_per_page; 
			}               
		}
		
        else  {
			$start = 0;
			$end = $entries_per_page; 
		}
						     
         	        
        // display user records in a table 
		echo "<div class=\"panel panel-default\">";

        echo "<table class=\"table table-striped\">";
        echo "<thead>
            <tr>
                <th>Research Area Keyword(s)</th>
                <th>User</th>
                <th>Upload Date</th>"; 
                echo "<th>View</th>";
                 
                echo "<th>Edit</th>";
              
                echo "<th>Delete</th>";
          echo "</tr></thead><tbody>";
    
      $row = $this->query->fetchAll();
      for ($i = $start; $i < $end; $i++)  {
        if ($i == $this->total_results) { break; }
    
        // display each section of information for the current row/record
        echo "<tr>";
        echo '<td>' . $row[$i]['keywords'] . '</td>';
        echo '<td>' . $row[$i]['username'] . '</td>';
        echo '<td>' . $row[$i]['uploadStamp'] . '</td>';

        // options to view, edit, or delete each record (using recordID to know which to delete, edit, or view)
        // Pass the ID of the record to know which one is being viewed.
        echo '<td><a href="view-record.php?id=' . $row[$i]['researchID'] . '">View Record</a></td>';
        
        // Only offer edit and delete option if no search taking place (can't allow user to edit or delete other researchers' files) or if it is the current user's files.
        if(!$this->search_mode || $row[$i]['username'] == $_SESSION['user'])  {
            echo '<td><a href="record.php?id=' . $row[$i]['researchID'] . '">Edit Record</a></td>';

        
            echo '<td><a onclick = "return confirm(\'Are you sure?\')" href="delete.php?id=' . $row[$i]['researchID'] . '">Delete Record</a></td>';
        }
        echo "</tr>"; 
      } 

      echo "</table></div>"; 
	  
	
    }
}


// ---------------------------------------------------------------------------------------------------------------------------------------------------------

class RecordView  {
    var $db;
    var $recordID;
    var $user;
    var $userID;
    
    function getID()  {
        return $this->recordID;
    }
    
    
    // Sets up db connection and gets info necessary to show the user the specified record.
    function getInformation()  {
        $this->db = dbConnect(); 
        
        // Get the ID of the record to be viewed.
        if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)  { 
            $this->recordID = checkForm($_GET['id']);
        }  
        
        else if(isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0)  {
            $this->recordID = checkForm($_POST['id']);
        }
        
        $this->user = $_SESSION['user'];;
        $this->userID = $_SESSION['userID'];
    }
    
    // Create a table with the current record to be viewed by the user.
    function createRecordView()  {
        $this->getInformation();
        
        // Get all the information associated with the record.
        $sql = 'SELECT * FROM research WHERE researchID = ?';
        $results = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $results->execute(array($this->recordID));
        
        
        
        echo "<div class=\"container\"><table class=\"table table-bordered table-striped\"><tbody>";
        
        // Print a pretty table of the information for the record.
        if($row = $results->fetch(PDO::FETCH_ASSOC))  {
            echo "<tr> <td>Research Area Keyword(s):</td> <td>" . $row['keywords'] . "</td> </tr>";
              $this->showUploadedAttachments();
              echo "</td></tr>";
        }
        
        echo "</tbody></table></div>";
        
        
       
    }   
    
    
    function showUploadedAttachments()  {
        // Save the current directory location.
        $current_dir = getcwd();
        
        // the path for the attachments of the given record
        $path = "../../uploads/{$this->recordID}/attachments/";
        
        // Make a copy of the path.
        $copy = $path;
        if(is_dir($path))  {
            // Change to the attachment directory for the record.
            chdir($path);
            
            // Get all attached files with the valid extensions.
            $path = glob("*.{pdf,PDF,DOCX,DOC,TXT,txt,doc,docx}", GLOB_BRACE);
            
            // Display a link to each attachment ($copy contains the path while $attached contains the name--this links to the actual file itself)
            foreach ($path as $attached) {
                echo "<a href=\"{$copy}{$attached}\">$attached</a>";
                echo "<br />";
            }
            
            // Restore the directory.
            chdir($current_dir);
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------


// generic helper function for security
// strip out any suspicious/annoying bits of input 
function checkForm($input) {
      $input = trim($input);
      $input = stripslashes($input);
      $input = htmlspecialchars($input);
      
      return $input;
}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

// generic helper function for database connection
function dbConnect()  {
    $hostname = "localhost";
    $username = "reswebuser";
    $password = "cloud";
    
    try {
        $db = new PDO("mysql:host=$hostname;dbname=resweb", $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }
    
    catch(PDOException $e) {
        echo $e->getMessage();
    }
}


// ------------------------------------------------------------------------------------------------------------------------------------------------------
?>
