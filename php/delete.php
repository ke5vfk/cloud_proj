<?php
  
  // for user session information across pages
  session_start();
  
  // If no user session in place, restrict user to landing page.
  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: login.php"); 
        die;
  } 

  require("form_processing_helpers.php");

  // get the ID of the record to be deleted
  if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)   {
      $ID = checkForm($_GET['id']);
    
      $user = $_SESSION['user'];
      $userID = $_SESSION['userID'];
     
      // connect to db
      $db = dbConnect();
      
      // purge the record from our db
      $sql = 'DELETE FROM research WHERE researchID = ? AND userID = ?';
      $deleteRecord = $db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
      $deleteRecord->execute(array($ID, $userID));
      
        
      // Don't just delete the record.  Delete any uploads associated with the record.
      if($deleteRecord)  {
            // The possible upload paths to look for with the record.
            $attach_path = "../../uploads/{$ID}/attachments/";
            
            // Check to see if any attachments uploaded.
            if(is_dir($attach_path))  {
              $files = glob("../../uploads/{$ID}/attachments/*"); 
              
              // Loop through and delete all files.
              foreach($files as $file) { 
                if(is_file($file))  {
                  unlink($file); 
                }
              } 
              
              // Remove the attachments directory.
              rmdir($attach_path);
            }
            
            // Finally, delete the ID directory (i.e. 22) for the record.
            if(is_dir($delete_dir))  {
              rmdir($delete_dir);
            }
      }


      // close the connection to db
      $db = null;

      // Redirect back to listing of all records.
      header("Location: view.php"); 
      die;
    }
  
?>

