<?php

  // for user session information across pages
  session_start();

  require("account_helpers.php");

  $create_user = new CreateUser;
  $create_user->addUser();
	include('nav.php');


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="../style/page.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <title>Create An Account</title>

  </head>

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

 

	<div class="panel panel-primary pass center-block center-all">
		<div class="panel-heading lead text-uppercase text-center">Create An Account</div>
		<div class="panel-body">

		  <form class="form-signin" role="form" method = "post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
				<div class="form-group">
					<label for="text" > Email Address* </label>
					<input type="email" class="form-control" id = "email" name = "email" onchange="checkEmail(this);" required autofocus onchange = "checkEmail();"><span class="error"><?php $create_user->getEmailErr();?></span>
				</div>
				<div class="form-group">
					<label for="text" > Password* </label>
					<input type="password" class="form-control" id = "createP" name = "createP" placeholder="Password" required onchange = "createPword(this);"><span class="error"><?php $create_user->getPwordErr();?></span>
				</div>
				<div class="form-group">
					<label for="text" > Confirm Password* </label>
					<input type="password" class="form-control" id = "createP2" name = "createP2" placeholder="Confirm Password" required onchange = "createPword2(this);"><span class="error"> <?php $create_user->getPword2Err();?></span>
				</div>
			<input type = "submit" class="btn btn-lg btn-primary btn-block" value = "Submit">
		  </form>
		</div>
    </div> <!-- /container -->

    <br /><br /><br /><br />

    <script src="../js/func.js"></script>
    
  </body>
</html>
