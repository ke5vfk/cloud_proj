<?php
  
  // for user session information across pages
  session_start();

  if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: landing.php"); 
        die;
  } 

  require("form_processing_helpers.php");

  $record = new ResultsView;
  
  // Initially, assume no search is happening and that we want to show all marks for the company.
  $record->search = false;
  
  
  // Check to see if we have any passed search data for view (meaning just show hits rather than all records).
  if(isset($_GET["clear"])){
		unset($_SESSION['post_data']);
		$record->search = false;
  }
  else if(isset($_SESSION['post_data']))  {
     $record->setupSearch($_SESSION['post_data']['query_string']);
     $record->search = true;
  } 
	$search = new Record;
	$search->processSearch();

	include('nav.php');


?>
<!DOCTYPE html>
<html lang="en">

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

  <div>  
	<div class="text-right">
		<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" >
			<input type="text" id="keywords" name="keywords" onchange="checkKeywords();" value ="<?php $search->getKeywords(); ?>" autofocus  size = 20>
			<input class="btn btn-primary" type="submit" name="submit_search"" id="submit_search" value="Search"><br/>
			<span><?php $search->getKeywordErr();?></span>
		</form>
	</div>
	<?php
		$record->getUserIDs();

		// Create a table of the trademark reports for the company.
		$record->createRecordTable();  
				$total_pages = $_SESSION['total_pages'];

			  echo '<div class="row panel-body">';
	  
	  echo "<div class='col-xs-8'><ul class='pagination'>";
        // Allow user to click to navigate between paginated view.
        for ($i = 1; $i <= $total_pages; $i++) {
			if (isset($_GET['page']) && $_GET['page'] == $i)  {
				echo $i . " ";
			}
            // Append the page ID to each URL.
			else  {
				echo "<li><a href='landing.php?page=$i'>$i</a></li>";
			}
		}
		
		echo "</ul>";
		echo '</div>
			<div class="col-xs-4 text-right">
			  <form class="form-signin" role="form" method = "post" action='. htmlspecialchars($_SERVER['PHP_SELF']).'>
				<input class="btn btn-primary " type="submit" name="submit_search"" id="clear_search" value="Clear Search" onclick="clearData()"><br/>
			  </form>
			</div></div>';	
	?>
	
<script>
	function clearData(){
		  var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	if (xhttp.readyState == 4 && xhttp.status == 200) {
		var newDoc = document.open("text/html", "replace");
		newDoc.write(xhttp.responseText);
		newDoc.close();
		
		}
	}

  xhttp.open("GET", "landing.php?clear=true", false);
  xhttp.send();

	}
</script>

 <script src="../js/func.js"></script>
    
  </body>
</html>
             