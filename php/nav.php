  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Research Hub</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../style/page.css">

  </head>

<nav class="navbar navbar-default">
  <div class="container-fluid">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span> 
	  </button>
	  <a class="navbar-brand" href="#">Research Hub</a>
	</div>
	<?php 
	if(isset($_SESSION['user']) && isset($_SESSION['userID'])){
		echo '<div class="collapse navbar-collapse" id="myNavbar">
				  <ul class="nav navbar-nav">
					<li id="home" onclick=class="inactive"><a href="landing.php">Home</a></li>
					<li id="record" class="inactive"><a href="record.php">New Submissions</a></li>
					<!--li id="view" class="inactive"><a href="view.php">Existing Record</a></li-->
					<!--li id="search" class="inactive"><a href = "search.php">Search Records</a></li-->
					<li id="pass" class="inactive"><a href = "changePword.php">Change Your Password</a></li>
				  </ul>
				  <ul class="nav navbar-nav navbar-right">
						<li class="inactive"><a href ="destroy.php" onclick = "alert(\"Logging out now...\");"><span class="glyphicon glyphicon-log-out">Logout</span></a></li>	  	
				  </ul>';
	}
	else{
		echo '<div class="collapse navbar-collapse" id="myNavbar">
				  <ul class="nav navbar-nav navbar-right">
					<li class="inactive"><a href ="login.php" ><span class="glyphicon glyphicon-log-in">Login</span></a></li>	  	
				  </ul>';
	}
	?>
	</div>
  </div>
</nav>