<?php
  session_start();

  require("account_helpers.php");

  $login=new Login;
  $login->account_login();
  	include('nav.php');

?>      

<!DOCTYPE html>
<html lang="en" >
  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
      <noscript>
          <p class="alert">*** Javascript required for this page. ***</p>
      </noscript>

		<div class="panel panel-primary login center-block center-all">
			<div class="panel-heading lead text-uppercase text-center ">Account Login</div>
			  <div class="panel-body">
				  <form role="form" class="form-signin" method = "post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
					<div class="form-group">
						<input type="email" class="form-control" id = "email" name = "email" placeholder="Email Address" required autofocus onchange = "checkEmail();"><span class="error"> <?php $login->getEmailErr();?></span>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id = "pword" name = "pword" placeholder="Password" required onchange = "checkPword();"><span class="error"> <?php $login->getPwordErr();?></span><br />
					</div>
					<a href = "forgot.php">Forgot Password</a><br /> 
					<a href = "create.php">Create An Account</a><br /><br />
					<input type ="submit" id="submit_login" name="submit_login" value = "Sign In">
				  </form>
				</div>
			</div>
		</div>

    <script src="../js/func.js"></script>
    
  </body>
</html>
