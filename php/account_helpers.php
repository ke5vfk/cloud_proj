<?php

// This file contains general PHP helper classes for user account related tasks (i.e. login, account creation, password recovery, etc.).

// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// used for user logins
class Login  {
    var $pwordErr;
    var $emailErr;
    var $email;
    var $pword;
    var $errors;
    var $login_attempts;
    var $user;
    var $password;
    var $db;
    var $newPass;
    var $new_pass_hash;
    
    // function to check a user's login credentials; if successful, start a new session
    // A maximum of 5 login attempts is allowed.  If 5 failures occur, an email will be sent to the user with a new password.
    // They will be locked out of their account until they recover using the new password.
    function account_login()  {

        // define error checking and container variables and set to empty values
        $this->emailErr = $this->pwordErr = "";
        $this->email = $this->pword = "";
        $this->errors = 0;
        $this->login_attempts = 0;

        if (isset($_POST['submit_login'])) {
            
            $this->db = dbConnect();
            
            // get the user's email address 
            $this->getEmail();

   
            // make sure password is supplied (not empty)
            $this->getPassword();


            // If so far, so good...
            if ($this->errors == 0)  {
                // Store the supplied email address.
                $this->user = $_POST['email'];

                // Store the supplied password.
                $this->password = $_POST['pword'];
  
                // Attempt to log the user into the system.
                $this->attemptLogin();
            }
        }
    }
    
    
    
    // Get the user-input email address.
    function getEmail()  {
        // regular expression for valid email
        $valid_email = "/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/";

        if ((empty($_POST["email"])) || (!(preg_match($valid_email, $_POST["email"])))) {
            $this->emailErr = "Please provide a valid email address.";
            $this->errors += 1;
        } 
    }
    
    
    
    
    // Get the user-input password (for account creation).
    function getPassword()  {
        if (empty(checkForm($_POST["pword"]))) {
            $this->pwordErr = "Please provide a valid password.";
            $this->errors += 1;
        }
    } 

    
    
    
    // Attempt to log the user in.  Only 5 failures may occur until user is locked out.
    function attemptLogin()  {
        // Locate user info in our db.
        $sql = 'SELECT * FROM users WHERE username = ?';
        $login = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $login->execute(array($this->user));
        
        
        if($login) {
            $row = $login->fetch(PDO::FETCH_ASSOC);
        }
  
        // Get the username.
        $ID = $row['username'];

        // Get the password for the user.
        $authentication = $row['password'];
        
        

        // Case 1:  valid username and password
        if($this->user == $ID AND password_verify($this->password, $authentication))  {
            $this->login_attempts = 0;
            
            // Start a session for this potential user login.
            $_SESSION['user'] = $this->user;
            $_SESSION['userID'] = $row['userID'];
            
        
            // Reset the login attempt counter to 0 upon successful login.
            // Update the date of last login to the current date.
            $sql = 'UPDATE users SET loginAttempts = ? WHERE username = ?';
            $resetLogins = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $resetLogins->execute(array($this->login_attempts, $this->user));

            // close db
            $this->db = null;
            
            // take user to home page
            header("Location: landing.php"); 
            die;
        }

        // Case 2:  Valid username only.
        elseif ($this->user == $ID) {
            // supply generic error
            $this->emailErr = "Invalid user email address or password!";

            // see how many failed logins have occurred
            $this->getLoginCount();

            if($this->login_attempts >= 4)  {
                // create new random password
                $this->newPass = substr(md5(uniqid(mt_rand(), true)), 0, 7);

                // encrypt this new random password
                $this->new_pass_hash = $this->blowfish_password();

                // Reset the number of failed login attempts to 0.
                $this->resetLoginCount();

                // Update database with new password.
                $this->updatePassword();

                // Send user a recovery email with the new password.
                $this->recoverPassword();

                // Inform user that they have been locked out and need to check their email.
                echo"
                        <script>
                            alert('Account lockout: too many failed logins. Check your email for account recovery.');
                            document.location.href = 'landing.php';
                        </script>";
            } 

            // otherwise, increment number of login failures
            else  {
                $sql = 'UPDATE users SET loginAttempts = loginAttempts + 1 WHERE username = ?';
                $update_login_attempts = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $update_login_attempts->execute(array($this->user));
            }
            
            $this->errors += 1;
        }

        // Case 3:  User completely nonexistent.
        else {
            $this->emailErr = "Invalid user email address or password!";
            $this->errors += 1;   
        }
    }


    
    
    
    // see how many failed logins have occurred
    function getLoginCount()  {        
        $sql = 'SELECT loginAttempts FROM users WHERE username = ?';
        $try_login = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $try_login->execute(array($this->user));
        
        $row = $try_login->fetch(PDO::FETCH_ASSOC);
        
        $this->login_attempts = $row['loginAttempts'];
    }
    
    
    
    
    // Encrypt the password supplied.
    function blowfish_password()  {
        // used for password_hash
        $cost = array(
            'cost' => 10
        );

        // encrypt the password using bcrypt and blowfish
        $password_hash = password_hash($this->newPass, PASSWORD_BCRYPT, $cost);
        return $password_hash;
    }
    
    
    
    
    // Reset the number of failed login attempts to 0 after locking user out.
    function resetLoginCount()  {
        $this->login_attempts = 0;
        
        $sql = 'UPDATE users SET loginAttempts = ? WHERE username = ?';
        $reset_attempts = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $reset_attempts->execute(array($this->login_attempts, $this->user));
    }
    
    
    
    
    // Update the user's password in the database.
    function updatePassword()  {
        $sql = 'UPDATE users SET password = ? WHERE username = ?';
        $updatePass = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $updatePass->execute(array($this->new_pass_hash, $this->user));
  
        if(!$updatePass)  {
            die ("Password insertion failed");
        }

        // close the db
        $this->db = null;
    }

    
    
    
    // NEEDS TESTING ON SERVER....NOT SURE HOW THIS LOOKS OR IF IT EVEN IS REALLY DOING WHAT IT'S SUPPOSED TO.  
    // Send an email to the user with their new password. 
    function recoverPassword()  {

        // subject
        $subject =  'Password Recovery';

        // message
        $message = 'Dear User,' . "\r\n\r\n" . 'You recently lost access to your Research Hub account.  Please attempt to login with 
            your new password to recover your account.  Your new login password is listed below:' . "\r\n\r\n\r\n" .
            'Password:  ' . $this->password . "\r\n\r\n\r\n" . 'Sincerely,' .
            "\r\n" . 'Research Hub';  

        // from
        $from = 'From: Research Hub <donotreply@researchhub.com>';


        // Mail to user.
        mail($this->user, $subject, $message, $from);
    }
    
    
    
    // Accessor function to get email error.
    function getEmailErr(){
       echo $this->emailErr;
    }
    
    
    
    // Accessor function to get password error.
    function getPwordErr(){
       echo $this->pwordErr;
    }   
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// used for password recovery
class ForgotPassword  {
    var $emailErr;
    var $errors;
    var $email;
    var $db;
    var $new_pass_hash;
    var $password;
    
    
    // If user has forgotten their password, they should probably change it.  So we are going to force a new password on the user (to encourage them
    // come up with a better one they can remember).  Change the password; then send user an email with the new password.
    function forgotPassword()  {
        $this->emailErr = "";
        $this->errors = 0;

       if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // connect to database
            $this->db = dbConnect();
            
            // get the user's email (required for forgotten password recovery)
            $this->getEmail();

            if ($this->errors == 0)  { 
                $this->email = $_POST['email'];
        

                // Searches for user in db.  Returns true if user found.
                if ($this->findUser())  {
                    //create random password
                    $this->password = substr(md5(uniqid(mt_rand(), true)), 0, 7); 

                    // encrypt this new random password
                    $this->new_pass_hash = $this->blowfish_password();

                    // Update database with new password.
                    $this->updatePassword();

                    // Send user a recovery email with the new password.
                    $this->recoverPassword();          

                    // Give recovery message.
                    echo"
                        <script>
                            alert('Please check your email for account recovery instructions.');
                            document.location.href = 'landing.php';
                        </script>";
                }
            }
        }
    }
    
    // Get the user-input email address.
    function getEmail()  {
        // regular expression for valid email
        $valid_email = "/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/";

        if ((empty($_POST["email"])) || (!(preg_match($valid_email, $_POST["email"])))) {
            $this->emailErr = "Please provide a valid email address.";
            $this->errors += 1;
        } 
    }
    
    
    // Return true if email matches one found in database.  Else provide error message and return false.
    function findUser()  {        
        $sql = 'SELECT username FROM users WHERE username = ?';
        $emailExists = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $emailExists->execute(array($this->email));
        
        $row = $emailExists->fetch(PDO::FETCH_ASSOC);
        
        $ID = $row['username'];
  
        if($this->email == $ID)  
            return true;

        else  {
            $this->emailErr = "Account not found.";
            $this->errors += 1;
            return false;
        }
    }
    
    
    // Encrypt the password supplied.
    function blowfish_password()  {
        // used for password_hash
        $cost = array(
            'cost' => 10
        );

        // encrypt the password using bcrypt and blowfish
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT, $cost);
        return $password_hash;
    }
    
    
     // Update the user's password in the database.
    function updatePassword()  {
        $sql = 'UPDATE users SET password = ? WHERE username = ?';
        $updatePass = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $updatePass->execute(array($this->new_pass_hash, $this->email));
  
        if(!$updatePass)  {
            die ("Password insertion failed");
        }

        // close the db
        $this->db = null;
    }
    
    
    // NEEDS TESTING ON SERVER....NOT SURE HOW THIS LOOKS OR IF IT EVEN IS REALLY DOING WHAT IT'S SUPPOSED TO.  
    // Send an email to the user with their new password. 
    function recoverPassword()  {

        // subject
        $subject =  'Password Recovery';

        // message
        $message = 'Dear User,' . "\r\n\r\n" . 'You recently lost access to your Research Hub account.  Please attempt to login with 
            your new password to recover your account.  Your new login password is listed below:' . "\r\n\r\n\r\n" .
            'Password:  ' . $this->password . "\r\n\r\n\r\n" . 'Sincerely,' .
            "\r\n" . 'Research Hub';  

        // from
        $from = 'From: Research Hub <donotreply@researchhub.com>';


        // Mail to user.
        mail($this->email, $subject, $message, $from);
    }
    
    // Accessor function to get email error.
    function getEmailErr(){
       echo $this->emailErr;
    }
    
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// used for changing an account's password
class ChangePassword  {
    var $pwordErr;
    var $newPwordErr;
    var $new2Err;
    var $errors;
    var $password;
    var $newPass;
    var $newPass2;
    var $db;
    var $email;
    var $new_pass_hash;
    
    
    // Change the user's password.
    function changePword()  {
        // create error checking and container variables and initialize
		$this->pwordErr = $this->newPwordErr = $this->new2Err = "";
    	$this->password = $this->newPass = $this->newPass2 = "";
    	$this->errors = 0;

        // if attempt made to submit the password change
    	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
          $this->db = dbConnect();

          // get the user's current password
          $this->getCurrentPassword();
      		
          // get the new password
          $this->getNewPassword();
      		
          // If user has not pissed off the system yet, confirm the new password.
          if($this->errors == 0)  
                $this->confirmNewPassword();
         

          // So far the user has not tried to bypass the system....continue on
          if($this->errors == 0)  {

              // The user is currently logged in if they are changing their password.
              // Find out what user is utilizing the session.
              // We need to know which user we are changing the password for...(and it's stupid to ask a user for their username when they're already logged in)
              $this->email = $_SESSION['user'];
  
              // Verify the user's current password.  If the user forgot to logout, don't give some random person the ability to change the password.
              $this->verifyPassword();
          }
        }
    }

    
    
    // User must supply current password before they are allowed to change their password.
    function getCurrentPassword()  {
        if (empty($_POST["pword"]))  {
            $this->pwordErr = "Please provide your current password.";
            $this->errors += 1;
        }
        else  {
            $this->password = $_POST['pword'];
        }
    }

    
    
    
    // Get the new password for the user account.
    function getNewPassword()  {
        if (empty($_POST["newPword"]))  {
            $this->newPwordErr = "Please provide a new password.";
            $this->errors += 1;
        }
        // sanitize the new password
        else  {
            $this->newPass = checkForm($_POST["newPword"]);
        }

        // user must confirm the new password
        if(empty($_POST["newPword2"]))  {
            $this->new2Err = "Please confirm your new password.";
            $this->errors += 1;
        }
        // also sanitize this input for security
        else  {
            $this->newPass2 = checkForm($_POST["newPword2"]);
        }
    }


    
    
    // Confirm the new password.
    function confirmNewPassword()  {
        if($this->newPass != $this->newPass2)  {
            $this->newPwordErr = $this->new2Err = "Passwords do not match.";
            $this->errors += 1;
        }
    }

    
    
    // Encrypt the password supplied.
    function blowfish_password()  {
        // used for password_hash
        $cost = array(
            'cost' => 10
        );

        // encrypt the password using bcrypt and blowfish
        $password_hash = password_hash($this->newPass, PASSWORD_BCRYPT, $cost);
        return $password_hash;
    }
    
    
    // Authenticate the user before allowing a password change.
    function verifyPassword()  {
        // Find the user's information in our db.
        $sql = 'SELECT username, password FROM users WHERE username = ?';
        $userExists = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $userExists->execute(array($this->email));
        
        $row = $userExists->fetch(PDO::FETCH_ASSOC);

        // Grab that username.
        $ID = $row['username'];

        // Grab that user's password from the db (for authentication).
        $authentication = $row['password'];

        // See if the user-input current password matches the one in our db.
        if (!(password_verify($this->password, $authentication)))  {
            $this->pwordErr = "Invalid password.";
            $this->errors += 1;
        }

        // If the passwords match, we need to update our db with the new password.
        else  {
            // Encrypt the new password.
            $this->new_pass_hash = $this->blowfish_password();
    
            // Update the password in the database.
            $this->updatePassword();
        }
    }
    
    
    
    
    // Update the user's password in the database.
    function updatePassword()  {
        $sql = 'UPDATE users SET password = ? WHERE username = ?';
        $updatePass = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $updatePass->execute(array($this->new_pass_hash, $this->email));
        
        if(!$updatePass)  {
            die ("Password insertion failed");
        }

        // close the db
        $this->db = null;
        
         header("Location: landing.php"); 
         die;
    }
    
    
    // Accessor function to get password error.  User cannot change their password if they don't authenticate.
    function getPwordErr(){
       echo $this->pwordErr;
    }
    
    // Accessor function to get new password error (i.e. passwords don't match, etc.).
    function getNewPwordErr(){
       echo $this->newPwordErr;
    }
    
    
    // Accessor function to get email error.
    function getNew2Err(){
       echo $this->new2Err;
    }
    
    

}


// ---------------------------------------------------------------------------------------------------------------------------------------------------------

// Class for the creation of new user accounts.
class CreateUser  {
    var $emailErr;
    var $pwordErr;
    var $pword2Err;
    var $email;
    var $pword;
    var $errors;
    var $db;
    var $user;
    var $password;
    var $password_hash;
    var $isCreate = false;
    
    
    // Function to add general users to company account (separate from admin)
    function addUser()  {
        // define error checking and container variables and set to empty values
  		$this->emailErr = $this->pwordErr = $this->pword2Err = "";
  		$this->email = $this->pword = $this->pword2 = "";
  		$this->errors = 0;
        $this->dbEmail = "";
         
         
        if(!$this->db) 
            $this->db = dbConnect();
          
        

        // if an attempt is made to submit the form
  		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

          // Get the user's email address.
          $this->getEmail();

          // If no errors yet found with the above required fields, go merrily on.
          if($this->errors == 0)  {

            // get username (email address) and password for the new user
    		$this->user = $_POST['email'];

            // Check for an existing account.
            $this->existingUser();
          }
        }
    }
    
    
    
    function showEmail()  {
        echo $this->email;
    }
    
    
    // Get the user-input email address.
    function getEmail()  {
        // regular expression for valid email
        $valid_email = "/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/";

        if ((empty($_POST["email"])) || (!(preg_match($valid_email, $_POST["email"])))) {
            $this->emailErr = "Please provide a valid email address.";
            $this->errors += 1;
        } 
    }
    
 
    // Get the user-input password (for account creation).
    function getPassword()  {
        if (empty(checkForm($_POST["createP"]))) {
            $this->pwordErr = "Please provide a valid password.";
            $this->errors += 1;
        }
    }
    
    
    // Confirm the password entry (i.e. for login, user enters password, then confirms it, no keyboard slip-ups)
    function confirmPassword()  {
        if (empty(checkForm($_POST["createP2"]))) {
            $this->pwordErr = "Please confirm your password.";
            $this->errors += 1;
        }  
    }
    
    
    // Matches the two inputted passwords -- more likely for user not to mess up.
    function matchPasswords()  {
        if($_POST["createP"] != $_POST["createP2"])  {
            $this->pwordErr = "The two passwords do not match.";
            $this->pword2Err = "The two passwords do not match.";
            $this->errors += 1;
        }
    }
    
    
    // Encrypt the password supplied.
    function blowfish_password()  {
        // used for password_hash
        $cost = array(
            'cost' => 10
        );

        // encrypt the password using bcrypt and blowfish
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT, $cost);
        return $password_hash;
    }
   
    
    // Function to check if general user account already exists (no duplicate general users).
    function existingUser()  {
        // Search to see if user already exists.
        $sql = 'SELECT username FROM users WHERE username = ?';
        $existingUser = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $existingUser->execute(array($this->user));
        
        $row = $existingUser->fetch(PDO::FETCH_ASSOC);
        $ID = $row['username'];
        
        // If user already exists, provide an error.
        if($ID == $this->user)  {
                $this->emailErr = "An account for this email address already exists.  Please try again.";
                $this->errors += 1;
        }
        
        // If no errors have been encountered, allow the new account to be added.
        if($this->errors == 0)  {
            $this->password = checkForm($_POST['createP']);
            $this->password_hash = $this->blowfish_password();
            $this->insertUser();
        }
    }
    
    
    
    // Insert a new general user.
    function insertUser()  {
           $sql = 'INSERT INTO users(username, password) VALUES(?,?)';
           $insertUser = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
           $insertUser->execute(array($this->user, $this->password_hash));
           
           $_SESSION['user'] = $this->user; 
            
           // Locate userID in our db.
           $sql = 'SELECT userID FROM users WHERE username = ?';
           $user = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
           $user->execute(array($this->user));
           $row = $user->fetch(PDO::FETCH_ASSOC);
            
           $_SESSION['user'] = $this->user;
           $_SESSION['userID'] = $row['userID'];
           
            // close the database
            $this->db = null;

            header("Location: landing.php"); 
            die;
    }
    
    
    // Accessor function to get password error.  User cannot change their password if they don't authenticate.
    function getEmailErr(){
       echo $this->emailErr;
    }
    
    // Accessor function to get new password error (i.e. passwords don't match, etc.).
    function getPwordErr(){
       echo $this->pwordErr;
    }
    
    // Accessor function to get new password error (i.e. passwords don't match, etc.).
    function getPword2Err(){
       echo $this->pword2Err;
    }
    
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------

// generic helper function for security
// strip out any suspicious/annoying bits of input 
function checkForm($input) {
      $input = trim($input);
      $input = stripslashes($input);
      $input = htmlspecialchars($input);
      return $input;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------

// generic helper function for database connection
function dbConnect()  {
    $hostname = "localhost";
    $username = "reswebuser";
    $password = "cloud";
    
    try {
        $db = new PDO("mysql:host=$hostname;dbname=resweb", $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }
    
    catch(PDOException $e) {
        echo $e->getMessage();
    }
}


?>