<?php

    session_start();
    
    if (!(isset($_SESSION['user'])) || !(isset($_SESSION['userID']))) {
        header("Location: login.php"); 
        die;
    }  
    
    // Unset any possible past search info (we're done doing a search if we're here).
    if(isset($_SESSION['post_data']))  {
      unset($_SESSION['post_data']);
    }
	
	include('nav.php');
?>

<!DOCTYPE html>
<html lang="en">

  <body>

      <!-- Tell script impaired users we *must* have Javascript -->
    <noscript>
      <p class="alert">*** Javascript required for this page. ***</p>
    </noscript>
		<!--nav class="navbar navbar-default">
			<div class="container-fluid">
				<a class="navbar-brand" href="#">Research Hub</a>
			</div>
			<div>
              <ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
                <li><a href="record.php">Submit New Research Area</a></li>
                <li><a href="view.php">View, Edit, or Delete an Existing Record</a></li>
                <li><a href = "search.php">Search Records</a></li>
                <li><a href = "changePword.php">Change Your Password</a></li>
				<li><a href ="destroy.php" onclick = "alert('Logging out now...');"><span class="glyphicon glyphicon-log-out">Logout</span></a></li>
              </ul>
			</div>
		</nav-->
    <br /><br />


    <script src="../js/func.js"></script>
    
  </body>
</html>