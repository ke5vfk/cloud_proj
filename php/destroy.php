<?php
    // Destroy the current user session on logout.
    session_start();
    session_destroy();

    // Redirect to login page for new potential user session.
    header("Location: login.php"); 
    die;

?>